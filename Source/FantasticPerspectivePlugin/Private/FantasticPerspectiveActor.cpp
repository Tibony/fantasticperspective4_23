// Copyright Aleksandr Krotov. All Rights Reserved.

#include "FantasticPerspectiveActor.h"

TMap<UWorld*, TArray<AFantasticPerspectiveActor*>> AFantasticPerspectiveActor::references;

AFantasticPerspectiveActor::AFantasticPerspectiveActor() : Super() {
	auto &refs = references.FindOrAdd(GetWorld());
	refs.AddUnique(this);
}

AFantasticPerspectiveActor::AFantasticPerspectiveActor(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer) {
	auto &refs = references.FindOrAdd(GetWorld());
	refs.AddUnique(this);
}

AFantasticPerspectiveActor::~AFantasticPerspectiveActor() {
	auto &refs = references.FindOrAdd(GetWorld());
	refs.Remove(this);
	if (refs.Num() <= 0)
		references.Remove(GetWorld());
}

void AFantasticPerspectiveActor::BeginPlay() {
	Super::BeginPlay();
	auto &refs = references.FindOrAdd(GetWorld());
	refs.AddUnique(this);
}

void AFantasticPerspectiveActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	auto &refs = references.FindOrAdd(GetWorld());
	refs.Remove(this);
	if (refs.Num() <= 0)
		references.Remove(GetWorld());
}
