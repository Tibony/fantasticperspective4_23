// Copyright Aleksandr Krotov. All Rights Reserved.

#include "FantasticPerspectiveFunctions.h"
#include "EngineUtils.h"
#include "FantasticPerspectiveLocalPlayer.h"

void UFantasticPerspectiveFunctions::ResetCache() {
	for (TObjectIterator<UFantasticPerspectiveLocalPlayer> i; i; ++i)
		i->ResetFantasticPerspectiveCache();
}

void UFantasticPerspectiveFunctions::ResetSettings(UPARAM(ref) FFantasticPerspectiveSettings &target) {
	target = FFantasticPerspectiveSettings();
}

const FFantasticPerspectiveFrustum UFantasticPerspectiveFunctions::GetFrustumSettings(const FFantasticPerspectiveSettings target) {
	return target.Frustum;
}

const FFantasticPerspectiveTransform UFantasticPerspectiveFunctions::GetTransformSettings(const FFantasticPerspectiveSettings target) {
	return target.Transform;
}

const FFantasticPerspectiveDebug UFantasticPerspectiveFunctions::GetDebugSettings(const FFantasticPerspectiveSettings target) {
	return target.Debug;
}

void UFantasticPerspectiveFunctions::SetFrustumSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveFrustum Frustum) {
	target.Frustum = Frustum;
}

void UFantasticPerspectiveFunctions::SetTransformSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveTransform Transform) {
	target.Transform = Transform;
}

void UFantasticPerspectiveFunctions::SetDebugSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveDebug Debug) {
	target.Debug = Debug;
}
