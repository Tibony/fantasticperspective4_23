// Copyright Aleksandr Krotov. All Rights Reserved.

#include "FantasticPerspectiveLocalPlayer.h"
#include "DrawDebugHelpers.h"
#include "SceneView.h"
#include "Engine.h"

void DrawDebugFrustumByProjectionData(const UWorld *World, const FSceneViewProjectionData &ProjectionData, const float MinZ, const float MaxZ, const FColor Color, const float Thickness) {
	FMatrix m = ProjectionData.ProjectionMatrix;

	const FVector2D orig(m.M[2][2], m.M[3][2]);
	if (ProjectionData.IsPerspectiveProjection()) {
		m.M[2][2] = MinZ / (MinZ - MaxZ);
		m.M[3][2] = -MaxZ * MinZ / (MinZ - MaxZ);
	}

	DrawDebugFrustum(World, m.Inverse()
		* (FTranslationMatrix(ProjectionData.ViewOrigin * -1.f)
			* ProjectionData.ViewRotationMatrix).Inverse(),
		Color, false, -1, 0, Thickness);
}

bool UFantasticPerspectiveLocalPlayer::GetProjectionData(FViewport* Viewport, EStereoscopicPass StereoPass, FSceneViewProjectionData& ProjectionData) const
{
	bool ret = Super::GetProjectionData(Viewport, StereoPass, ProjectionData);

	if (ret) {
		FFantasticPerspectiveSettings s;

		if (GetFantasticPerspectiveSettings(s)) {
			FMatrix &m = ProjectionData.ProjectionMatrix;

			// Debug Draw Original Frustum
			if (s.Debug.DrawOriginalFrustum) {
				const float MinZ = m.M[3][2];
				const float MaxZ = s.Debug.FrustumDepth;

				DrawDebugFrustumByProjectionData(GetWorld(), ProjectionData,
					MinZ, MaxZ,	s.Debug.OriginalFrustumColor, s.Debug.LineThickness);
			}

			// Aspect scale before applying another effects
			m.M[0][0] /= s.Frustum.PreAspectScale.X;
			m.M[1][1] /= s.Frustum.PreAspectScale.Y;

			// Dolly Zoom (Trombone)
			if (ProjectionData.IsPerspectiveProjection()
				&& !FMath::IsNearlyZero(s.Frustum.DollyZoom.FocalDistance)
				&& !FMath::IsNearlyZero(s.Frustum.DollyZoom.FocalDistance + s.Frustum.DollyZoom.DollyZoom))
			{
				const float Scale = s.Frustum.DollyZoom.FocalDistance /
					(s.Frustum.DollyZoom.FocalDistance + s.Frustum.DollyZoom.DollyZoom);
				m.M[0][0] /= Scale;
				m.M[1][1] /= Scale;
				s.Transform.LocalTranslation.X -= s.Frustum.DollyZoom.DollyZoom;
				if (s.Frustum.DollyZoom.DollyZoom > 0.f)
					m.M[3][2] += s.Frustum.DollyZoom.DollyZoom;
			}

			// Some constants and magic vector
			const float FarClipPlane = -m.M[3][2] / m.M[2][2];
			const float NearClipPlane = (1.f - m.M[3][2]) / m.M[2][2];
			const FVector2D Magic = FVector2D(
				m.M[0][0],
				s.Frustum.CompensateAspectRatio ? m.M[0][0] : m.M[1][1]
			) * (ProjectionData.IsPerspectiveProjection()
				? 2.f / (s.Frustum.CompensateFOV ? m.M[0][0] : 1.f) : .5f);
			const float Depth = ProjectionData.IsPerspectiveProjection()
				? 1.f : (FarClipPlane - NearClipPlane);

			// Lens Shift
			m.M[2][0] += -s.Frustum.LensShift.X * Magic.X;
			m.M[2][1] += -s.Frustum.LensShift.Y * Magic.Y;

			// Make Lens Tilt seamless with Lens Shift (For NVIDIA Ansel)
			if (s.Frustum.SeamlessLensTilt) {
				s.Frustum.Skew.X += s.Frustum.LensTilt.Y * m.M[2][0] / m.M[1][1];
				s.Frustum.Skew.Y += s.Frustum.LensTilt.X * m.M[2][1] / m.M[0][0];
				s.Frustum.PostAspectScale.X /= 1.f + s.Frustum.LensTilt.X * s.Frustum.PostAspectScale.X / m.M[0][0] * m.M[2][0];
				s.Frustum.PostAspectScale.Y /= 1.f + s.Frustum.LensTilt.Y * s.Frustum.PostAspectScale.Y / m.M[1][1] * m.M[2][1];
			}

			// Lens Tilt
			m.M[0][3] += s.Frustum.LensTilt.X / Depth;
			m.M[1][3] += s.Frustum.LensTilt.Y / Depth;

			// Position Shift
			m.M[3][0] += -s.Frustum.PositionShift.X * m.M[0][0];
			m.M[3][1] += -s.Frustum.PositionShift.Y * m.M[1][1];

			// Skew
			m.M[1][0] += s.Frustum.Skew.X * (s.Frustum.CompensateAspectRatio ? m.M[1][1] : m.M[0][0]);
			m.M[0][1] += s.Frustum.Skew.Y * m.M[0][0];

			// Clipping Plane Skew
			m.M[0][2] += s.Frustum.ClippingPlaneSkew.X / Depth;
			m.M[1][2] += s.Frustum.ClippingPlaneSkew.Y / Depth;

			// Aspect scale after applying another effects
			m.M[0][0] /= s.Frustum.PostAspectScale.X;
			m.M[1][1] /= s.Frustum.PostAspectScale.Y;

			// View Origin World Offset
			ProjectionData.ViewOrigin += s.Transform.ViewOriginWorldOffset;

			// View Origin World Rotation
			if (!s.Transform.WorldRotation.IsNearlyZero())
				ProjectionData.ViewRotationMatrix = (
					FRotationMatrix(s.Transform.WorldRotation * -1.f)
					* ProjectionData.ViewRotationMatrix
					);

			// View Origin World Translation
			if (!s.Transform.WorldTranslation.IsNearlyZero())
				ProjectionData.ViewRotationMatrix = (
					FTranslationMatrix(s.Transform.WorldTranslation * -1.f)
					* ProjectionData.ViewRotationMatrix);

			// View Origin Local Translation
			if (!s.Transform.LocalTranslation.IsNearlyZero())
				ProjectionData.ViewRotationMatrix = (
					ProjectionData.ViewRotationMatrix
					* FTranslationMatrix(FVector(
						s.Transform.LocalTranslation.Y,
						s.Transform.LocalTranslation.Z,
						s.Transform.LocalTranslation.X
					) * -1.f));

			// View Origin Local Rotation
			if (!s.Transform.LocalRotation.IsNearlyZero())
				ProjectionData.ViewRotationMatrix = (
					ProjectionData.ViewRotationMatrix
					* FRotationMatrix(FRotator(
						s.Transform.LocalRotation.Yaw,
						s.Transform.LocalRotation.Roll,
						s.Transform.LocalRotation.Pitch * -1.f
					)));

			// Debug Draw Adjusted Frustum
			if (s.Debug.DrawAdjustedFrustum) {
				const float MinZ = m.M[3][2];
				const float MaxZ = s.Debug.FrustumDepth +
					(FMath::IsNearlyZero(s.Frustum.DollyZoom.FocalDistance + s.Frustum.DollyZoom.DollyZoom)
						|| FMath::IsNearlyZero(s.Frustum.DollyZoom.FocalDistance)
						|| s.Frustum.DollyZoom.DollyZoom < 0
						? 0.f : s.Frustum.DollyZoom.DollyZoom);

				DrawDebugFrustumByProjectionData(GetWorld(), ProjectionData,
					MinZ, MaxZ, s.Debug.AdjustedFrustumColor, s.Debug.LineThickness);
			}
		}
	}

	return ret;
}

void UFantasticPerspectiveLocalPlayer::ResetFantasticPerspectiveCache() {
	auto &cache = FantasticPerspectiveCache;
	cache.ViewTarget = nullptr;
	cache.PlayerController = nullptr;
	cache.ViewTargetComponent = nullptr;
	cache.PlayerControllerComponent = nullptr;
}

template <typename T>
inline bool IsFullValid(T* ptr) {
	if (ptr == nullptr)
		return false;
	if (!ptr->IsValidLowLevelFast())
		return false;
	if (!ptr->IsValidLowLevel())
		return false;
	return !ptr->IsPendingKill();
}

template <typename T>
inline T* MakeValid(T* ptr) {
	return (IsFullValid(ptr) ? ptr : nullptr);
}

template <typename T>
inline bool Validate(T* &ptr) {
	if (IsFullValid(ptr))
		return true;
	ptr = nullptr;
	return false;
}

bool UFantasticPerspectiveLocalPlayer::GetFantasticPerspectiveSettings(FFantasticPerspectiveSettings &outSettings) const
{
	auto &cache = FantasticPerspectiveCache;

	if (cache.ViewTarget != PlayerController->GetViewTarget())
	{
		cache.ViewTarget = MakeValid(PlayerController->GetViewTarget());
		cache.ViewTargetComponent = MakeValid(Cast<UFantasticPerspectiveComponent>(IsFullValid(cache.ViewTarget) ?
			cache.ViewTarget->GetComponentByClass(UFantasticPerspectiveComponent::StaticClass()) : nullptr));
	}

	if (Validate(cache.ViewTarget) && Validate(cache.ViewTargetComponent)) {
		outSettings = cache.ViewTargetComponent->Settings;
		return true;
	}

	if (cache.PlayerController != PlayerController)
	{
		cache.PlayerController = MakeValid(PlayerController);
		cache.PlayerControllerComponent = MakeValid(Cast<UFantasticPerspectiveComponent>(IsFullValid(cache.PlayerController) ?
			cache.PlayerController->GetComponentByClass(UFantasticPerspectiveComponent::StaticClass()) : nullptr));
	}
	
	if (Validate(cache.PlayerController) && Validate(cache.PlayerControllerComponent)) {
		outSettings = cache.PlayerControllerComponent->Settings;
		return true;
	}

	auto *refs = AFantasticPerspectiveActor::references.Find(GetWorld());
	if (refs)
		for (int i = refs->Num() - 1; i >= 0; i--) {
			if (IsFullValid(refs->operator[](i))) {
				outSettings = refs->operator[](i)->Settings;
				return true;
			}
		}

	return false;
}
