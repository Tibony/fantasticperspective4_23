// Copyright Aleksandr Krotov. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FantasticPerspectiveSettings.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FantasticPerspectiveFunctions.generated.h"

UCLASS()
class FANTASTICPERSPECTIVEPLUGIN_API UFantasticPerspectiveFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "FantasticPerspective|Settings")
	static void ResetCache();
	
	UFUNCTION(BlueprintCallable, Category = "FantasticPerspective|Settings")
	static void ResetSettings(UPARAM(ref) FFantasticPerspectiveSettings &target);

	UFUNCTION(BlueprintPure, Category = "FantasticPerspective|Settings")
	static const FFantasticPerspectiveFrustum GetFrustumSettings(const FFantasticPerspectiveSettings target);

	UFUNCTION(BlueprintPure, Category = "FantasticPerspective|Settings")
	static const FFantasticPerspectiveTransform GetTransformSettings(const FFantasticPerspectiveSettings target);

	UFUNCTION(BlueprintPure, Category = "FantasticPerspective|Settings")
	static const FFantasticPerspectiveDebug GetDebugSettings(const FFantasticPerspectiveSettings target);

	UFUNCTION(BlueprintCallable, Category = "FantasticPerspective|Settings")
	static void SetFrustumSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveFrustum Frustum);

	UFUNCTION(BlueprintCallable, Category = "FantasticPerspective|Settings")
	static void SetTransformSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveTransform Transform);

	UFUNCTION(BlueprintCallable, Category = "FantasticPerspective|Settings")
	static void SetDebugSettings(UPARAM(ref) FFantasticPerspectiveSettings &target, const FFantasticPerspectiveDebug Debug);
};
