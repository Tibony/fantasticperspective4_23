// Copyright Aleksandr Krotov. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FantasticPerspectiveSettings.generated.h"

USTRUCT(BlueprintType)
struct FFantasticPerspectiveDollyZoom {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DollyZoom")
		float DollyZoom = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="DollyZoom")
		float FocalDistance = 0.f;
};

USTRUCT(BlueprintType)
struct FFantasticPerspectiveFrustum {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D LensShift = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D LensTilt = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D PositionShift = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D Skew = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D PreAspectScale = FVector2D::UnitVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D PostAspectScale = FVector2D::UnitVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FVector2D ClippingPlaneSkew = FVector2D::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		FFantasticPerspectiveDollyZoom DollyZoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		bool CompensateAspectRatio = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		bool CompensateFOV = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Frustum")
		bool SeamlessLensTilt = true;
};

USTRUCT(BlueprintType)
struct FFantasticPerspectiveTransform {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transform")
		FVector ViewOriginWorldOffset = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transform")
		FVector WorldTranslation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transform")
		FRotator WorldRotation = FRotator::ZeroRotator;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transform")
		FVector LocalTranslation = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Transform")
		FRotator LocalRotation = FRotator::ZeroRotator;
};

USTRUCT(BlueprintType)
struct FFantasticPerspectiveDebug {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		bool DrawOriginalFrustum = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		FColor OriginalFrustumColor = FColor::Blue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		bool DrawAdjustedFrustum = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		FColor AdjustedFrustumColor = FColor::Red;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		float LineThickness = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Debug")
		float FrustumDepth = 1000.f;
};

USTRUCT(BlueprintType)
struct FFantasticPerspectiveSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
		FFantasticPerspectiveFrustum Frustum;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
		FFantasticPerspectiveTransform Transform;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
		FFantasticPerspectiveDebug Debug;
};
