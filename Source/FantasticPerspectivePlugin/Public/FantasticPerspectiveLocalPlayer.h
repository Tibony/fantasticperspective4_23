// Copyright Aleksandr Krotov. All Rights Reserved.

#pragma once

#include "SceneView.h"
#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "FantasticPerspectiveActor.h"
#include "FantasticPerspectiveSettings.h"
#include "FantasticPerspectiveComponent.h"
#include "FantasticPerspectiveLocalPlayer.generated.h"

UCLASS(BlueprintType)
class UFantasticPerspectiveLocalPlayer : public ULocalPlayer
{
	GENERATED_BODY()

	mutable struct {
		APlayerController* PlayerController = nullptr;
		UFantasticPerspectiveComponent* PlayerControllerComponent = nullptr;
		AActor* ViewTarget = nullptr;
		UFantasticPerspectiveComponent* ViewTargetComponent = nullptr;
		AFantasticPerspectiveActor* kal = nullptr;
	} FantasticPerspectiveCache;

public:
	bool GetFantasticPerspectiveSettings(FFantasticPerspectiveSettings &outSettings) const;
	void ResetFantasticPerspectiveCache();
	virtual bool GetProjectionData(FViewport* Viewport, EStereoscopicPass StereoPass, FSceneViewProjectionData& ProjectionData) const override;
};
