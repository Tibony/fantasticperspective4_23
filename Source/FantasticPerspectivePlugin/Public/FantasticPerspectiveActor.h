// Copyright Aleksandr Krotov. All Rights Reserved.

#pragma once

#include "Engine.h"
#include "CoreMinimal.h"
#include "FantasticPerspectiveSettings.h"
#include "FantasticPerspectiveActor.generated.h"

UCLASS(Blueprintable, meta = (DisplayName = "FantasticPerspectiveManager"))
class AFantasticPerspectiveActor : public AActor
{
	GENERATED_BODY()

	static TMap<UWorld*, TArray<AFantasticPerspectiveActor*>> references;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FantasticPerspective")
		FFantasticPerspectiveSettings Settings;

	AFantasticPerspectiveActor(); 
	AFantasticPerspectiveActor(const FObjectInitializer& ObjectInitializer);
	~AFantasticPerspectiveActor();

	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	friend class UFantasticPerspectiveLocalPlayer;
};
