// Copyright Aleksandr Krotov. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "FantasticPerspectiveSettings.h"
#include "FantasticPerspectiveComponent.generated.h"

UCLASS(Blueprintable, meta = (DisplayName="FantasticPerspectiveManager", BlueprintSpawnableComponent))
class UFantasticPerspectiveComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FantasticPerspective")
		FFantasticPerspectiveSettings Settings;
};
